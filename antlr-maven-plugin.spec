Name:          antlr-maven-plugin
Version:       2.2
Release:       1
Summary:       Maven plugin that generates files based on grammar file(s)
License:       ASL 2.0
URL:           https://www.antlr.org/api/maven-plugin/latest
Source0:       https://repo1.maven.org/maven2/org/codehaus/mojo/antlr-maven-plugin/2.2/antlr-maven-plugin-2.2-source-release.zip
Patch0:        maven-antlr-plugin-2.2-modello-issue.patch
Patch2:        maven-antlr-plugin-2.1-sinkfix.patch
Patch3:        0001-MANTLR-34-Fix-NPE-when-building-Jenkins.patch
BuildArch:     noarch
BuildRequires: maven-local mvn(org.apache.commons:commons-exec)
BuildRequires: mvn(org.apache.maven:maven-plugin-api) mvn(org.apache.maven:maven-project)
BuildRequires: mvn(org.apache.maven.plugins:maven-plugin-plugin)
BuildRequires: mvn(org.apache.maven.reporting:maven-reporting-impl)
BuildRequires: mvn(org.apache.maven.shared:maven-plugin-testing-harness)
BuildRequires: mvn(org.apache.maven.wagon:wagon-provider-api)
BuildRequires: mvn(org.codehaus.modello:modello-maven-plugin)
BuildRequires: mvn(org.codehaus.mojo:mojo-parent:pom:) mvn(org.codehaus.plexus:plexus-i18n)
BuildRequires: mvn(org.codehaus.plexus:plexus-utils)

%description
The Antlr Plugin has two goals:
- antlr:generate Generates file(s) to a target directory based on grammar
  file(s).
- antlr:html Generates Antlr report for grammar file(s).

%package javadoc
Summary:       Javadocs for %{name}
%description javadoc
This package contains the API documentation for %{name}.

%prep
%setup -q
%patch0 -p1 -b .modello
%patch2 -b .sink
%patch3 -p1 -b .fixnpe
%pom_xpath_inject "pom:dependency[pom:artifactId[text()='maven-reporting-impl']]/pom:exclusions" "
        <exclusion>
            <groupId>antlr</groupId>
            <artifactId>antlr</artifactId>
        </exclusion>"
find -name '*.class' -exec rm -f '{}' \;
find -name '*.jar' -exec rm -f '{}' \;
%mvn_file : %{name}

%build
%mvn_build -- -Dmaven.test.skip=true

%install
%mvn_install

%files -f .mfiles

%files javadoc -f .mfiles-javadoc

%changelog
* Mon Jul 27 2020 chengzihan <chengzihan2@huawei.com> - 2.2-1
- Package init
